#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard
from trytond.pyson import Bool, Equal, Eval
from trytond.pool import Pool
from trytond.transaction import Transaction


_STATES = {
    'readonly': Equal(Eval('state'), 'done'),
}
_DEPENDS = ['state']


class PaymentLine(ModelSQL, ModelView):
    'Payment Line'
    _name = 'account.invoice.payment_line'
    _description = __doc__
    _rec_name = 'date'

    invoice = fields.Many2One('account.invoice', 'Invoice', required=True,
            on_change=['invoice'], domain=[('state', 'in', ('draft', 'open'))],
            states=_STATES, depends=_DEPENDS)
    amount = fields.Numeric('Amount', digits=(16, Eval('currency_digits', 2)),
            required=True, depends=_DEPENDS+['currency_digits'],
            states=_STATES)
    currency_digits = fields.Function(fields.Integer('Currency Digits',
            on_change_with=['currency']), 'get_currency_digits', )
    date = fields.Date('Date', required=True, states=_STATES, depends=_DEPENDS)
    currency = fields.Many2One('currency.currency', 'Currency', required=True,
            states=_STATES, depends=_DEPENDS)
    description = fields.Char('Description', states=_STATES, depends=_DEPENDS,
            required=True)
    journal = fields.Many2One('account.journal', 'Journal', required=True,
            domain=[('type','=','cash')], on_change=['journal'],
            states=_STATES, depends=_DEPENDS)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('done', 'Done')
            ], 'State', required=True, readonly=True)
    move_line = fields.Many2One('account.move.line', 'Move Line', readonly=True,
            states={'required': Bool(Equal(Eval('state'), 'done'))},
            depends=['state'])

    def default_state(self):
        return 'draft'

    def on_change_invoice(self, vals):
        invoice_obj = Pool().get('account.invoice')

        res={}
        if vals.get('invoice'):
            invoice = invoice_obj.browse(vals['invoice'])
            res['amount'] = invoice.amount_to_pay
            res['currency'] = invoice.currency.id
            res['currency_digits'] = invoice.currency.digits
        return res

    def on_change_journal(self, vals):
        journal_obj = Pool().get('account.journal')

        res = {'description': False}
        if vals.get('journal'):
            journal = journal_obj.browse(vals['journal'])
            res['description'] = journal.name
        return res

    def get_currency_digits(self, ids, name):
        res = {}
        for line in self.browse(ids):
            if line.currency:
                res[line.id] = line.currency.digits
            else:
                res[line.id] = 2
        return res

    def on_change_with_currency_digits(self, vals):
        currency_obj = Pool().get('currency.currency')
        if vals.get('currency'):
            currency = currency_obj.browse(vals['currency'])
            return currency.digits
        return 2

PaymentLine()


class ProceedPayments(Wizard):
    'Proceed Payments'
    _name = 'account.invoice.payment_line.proceed_payments'

    states = {

        'init': {
            'result': {
                'type': 'action',
                'action': '_proceed_payments',
                'state': 'end',
                },
            },
        }

    def __init__(self):
        super(ProceedPayments, self).__init__()
        self._error_messages.update({
            'amount_greater_amount_to_pay': 'The amount of the payment line for ' \
                    'invoice "%s" exceeds the amount (%s) to pay!\n' \
                    'Please adjust the amount or delete the line.',
            'invoice_not_opened': 'The invoice "%s" is not opened!\n' \
                    'Payments can only be proceeded on opened invoices.',
            })

    def _proceed_payments(self, data):
        pool = Pool()
        payment_line_obj = pool.get('account.invoice.payment_line')
        invoice_obj = pool.get('account.invoice')
        currency_obj = pool.get('currency.currency')
        move_line_obj = pool.get('account.move.line')

        line_ids = payment_line_obj.search([('state', '=', 'draft')])
        payment_lines = payment_line_obj.browse(line_ids)

        res = {}
        for line in payment_lines:
            invoice = invoice_obj.browse(line.invoice.id)
            if invoice.state != 'open':
                self.raise_user_error('invoice_not_opened',
                        (line.invoice.rec_name))
                continue
            with Transaction().set_context(date=invoice.currency_date):
                amount = currency_obj.compute(line.currency.id, line.amount,
                    invoice.company.currency.id)

            reconcile_lines = invoice_obj.get_reconcile_lines_for_amount(
                    invoice, amount)

            amount_second_currency = False
            second_currency = False
            if line.currency.id != invoice.company.currency.id:
                amount_second_currency = line.amount
                second_currency = line.currency.id

            if amount > invoice.amount_to_pay:
                self.raise_user_error('amount_greater_amount_to_pay',
                        (line.invoice.rec_name, invoice.amount_to_pay))

            line_id = False
            if not currency_obj.is_zero(invoice.company.currency, amount):
                line_id = invoice_obj.pay_invoice(line.invoice.id, amount,
                        line.journal.id, line.date, line.description or '',
                        amount_second_currency, second_currency)

            payment_line_obj.write(line.id, {
                    'state': 'done',
                    'move_line': line_id
                    })
            if reconcile_lines[1] != Decimal('0.0'):
                pass
            else:
                line_ids = reconcile_lines[0]
                if line_id:
                    line_ids += [line_id]
                if line_ids:
                    move_line_obj.reconcile(line_ids)
        return res

ProceedPayments()
