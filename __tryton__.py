#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Invoice Payment Batch Capture',
    'name_de_DE': 'Fakturierung Rechnungsstellung Stapelerfassung Zahlungen',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz/',
    'description': '''Account Invoice Payment Batch Capture
    - Provides the recording and direct processing of batches of payments for invoices
''',
    'description_de_DE': '''Fakturierung Rechnungsstellung Stapelerfassung von Zahlungen
    - Erlaubt die Stapelerfassung und direkte Verarbeitung von Zahlungen für Rechnungen.
''',
    'depends': [
        'account_invoice',
    ],
    'xml': [
        'payment.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
