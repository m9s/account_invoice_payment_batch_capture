# 
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "error:account.invoice.payment_line.proceed_payments:0"
msgid ""
"The amount of the payment line for invoice \"%s\" exceeds the amount (%s) to pay!\n"
"Please adjust the amount or delete the line."
msgstr ""
"Der Zahlungsbetrag für Rechnung \"%s\" überschreitet den fälligen Betrag (%s)!\n"
"Bitte korrigieren Sie den Betrag oder löschen Sie die Zeile."

msgctxt "error:account.invoice.payment_line.proceed_payments:0"
msgid ""
"The invoice \"%s\" is not opened!\n"
"Payments can only be proceeded on opened invoices."
msgstr ""
"Die Rechnung \"%s\" ist nicht fakturiert!\n"
"Zahlungen können nur für fakturierte Rechnungen vorgenommen werden."

msgctxt "field:account.invoice.payment_line,amount:0"
msgid "Amount"
msgstr "Betrag"

msgctxt "field:account.invoice.payment_line,currency:0"
msgid "Currency"
msgstr "Währung"

msgctxt "field:account.invoice.payment_line,currency_digits:0"
msgid "Currency Digits"
msgstr "Währung (signifikante Stellen)"

msgctxt "field:account.invoice.payment_line,date:0"
msgid "Date"
msgstr "Datum"

msgctxt "field:account.invoice.payment_line,description:0"
msgid "Description"
msgstr "Bezeichnung"

msgctxt "field:account.invoice.payment_line,invoice:0"
msgid "Invoice"
msgstr "Rechnung"

msgctxt "field:account.invoice.payment_line,journal:0"
msgid "Journal"
msgstr "Journal"

msgctxt "field:account.invoice.payment_line,move_line:0"
msgid "Move Line"
msgstr "Buchungszeile"

msgctxt "field:account.invoice.payment_line,rec_name:0"
msgid "Name"
msgstr "Name"

msgctxt "field:account.invoice.payment_line,state:0"
msgid "State"
msgstr "Status"

msgctxt "model:account.invoice.payment_line,name:0"
msgid "Payment Line"
msgstr "Zahlungsposition"

msgctxt "model:ir.action,name:act_payment_line"
msgid "Batch Capture of Cash Payments"
msgstr "Stapelerfassung von Barzahlungen"

msgctxt "model:ir.action,name:wizard_proceed_payments"
msgid "Proceed Payments"
msgstr "Zahlungen verarbeiten"

msgctxt "model:ir.ui.menu,name:menu_payment"
msgid "Payment"
msgstr "Zahlung"

msgctxt "model:ir.ui.menu,name:menu_payment_tree"
msgid "Batch Capture of Cash Payments"
msgstr "Stapelerfassung von Barzahlungen"

msgctxt "selection:account.invoice.payment_line,state:0"
msgid "Done"
msgstr "Erledigt"

msgctxt "selection:account.invoice.payment_line,state:0"
msgid "Draft"
msgstr "Entwurf"

msgctxt "view:account.invoice.payment_line:0"
msgid "Payment Lines"
msgstr "Zahlungspositionen"
